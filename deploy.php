<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html lang="en">
    <head>
        <title>Deploy</title>

        <script>
            function onCheckPatchTypes (type) {
                var elements = document.getElementsByClassName(type);
                var main_checker = document.getElementById(type);
                for(var i=0;i<elements.length;i++){
                    if (main_checker.checked) {
                        elements[i].checked = 'checked';
                    } else {
                        elements[i].checked = '';
                    }
                }
            }
        </script>

    </head>
    <body>
        <div id="tabs">
            <div id="update_db">
                <?php
                $fileTypes = array("Structure install" => 'install',
                    "Structure patch" => 'patch',
                    "Data install" => 'datainstall',
                    "Data patch" => 'datapatch',
                    "TEST data install" => 'testdatai',
                    "TEST data patch" => 'testdatap');

                define ('BASEPATH', dirname (__FILE__) . '/');
                define ('SQL_DIR', BASEPATH . 'sql/');
                define ('SQL_FILENAME_PATTERN', "#\d{4}\.\d{2}\.\d{2}\.\d{4}\.(" . implode ("|", $fileTypes) . ")\.\w+?\.sql#si");
                $FILES_TO_SKIP = array('.', '..', '_svn', '.svn', '.git');
                $PRECHECKED_TYPES = array('patch', 'datapatch');

                echo "<form method=\"get\"><input type=\"hidden\" name=\"tab\" value=\"update_db\" /><table><tr>";
                $index = 0;
                foreach ($fileTypes as $typeName => $fileType) {
                    //define if check by default
                    $checked = '';
                    if (in_array ($fileType, $PRECHECKED_TYPES)
                        || (isset ($_GET['types']) && in_array ($fileType, $_GET['types']))
                    ) {
                        $checked = 'checked="checked"';
                    }
                    if ($index++ % 2 == 0) {
                        echo "</tr><tr>";
                    }
                    echo "<td><label>$typeName</label>:</td><td width=\"100\">
        <input onclick=\"onCheckPatchTypes('" . $fileType . "')\" id=\"" . $fileType . "\" type=\"checkbox\" " . $checked . " name=\"types[]\" value=\"" . $fileType . "\" />
        </td>";
                }
                echo '</tr></table><br />';
                $FILES = render_patches_list ();
                echo '<input type="submit" name="s" value="Execute" />&nbsp;&nbsp;&nbsp;<a href="?reset">Reset</a></form>';

                if (!empty ($_GET)) {
                    echo "<hr /><br /><b>Starting to apply patches</b><br /><hr />";

                /** REMOVE CODE THAT IS NOT NEEDED FOR EITHER YII OR CI **/

                    /**
                     * load DB settings for YII
                     */
                    if (file_exists ('./protected/config/custom.php'))
                        require('./protected/config/custom.php');
                    else {
                        require('./protected/config/main.php');
                        $db = $config['components']['db'];
                    }
                    $db['parts'] = explode (';', $db['connectionString']);
                    $db['host']  = explode ('=', $db['parts'][0]);
                    $db['name']  = explode ('=', $db['parts'][1]);
                    mysql_connect($db['host'][1], 
                                  $db['username'], 
                                  $db['password']);

                    mysql_select_db ($db['name'][1]);

                    /**
                     * load DB settings for CodeIgniter
                     */
                    // load all settings
                    require('./application/config/config.php');
                    // ... and DB vars
                    require('./application/config/database.php');

                    mysql_connect($db['default']['hostname'],
                                  $db['default']['username'],
                                  $db['default']['password']);

                    mysql_select_db($db['default']['database']);

                /** STOP REMOVING CODE THAT IS NOT NEEDED FOR EITHER YII OR CI **/

                    /*
                     * Sort file by name
                     */
                    sort ($FILES, SORT_STRING);

                    /*
                     * Pay attention: we sorted files list before this
                     */
                    foreach ($FILES as $file) {
                        if ($sql = patch_availible ($file)) {
                            apply_patch ($sql, $file);
                        }
                    }
                }

                function render_patches_list ()
                {
                    global $FILES_TO_SKIP, $PRECHECKED_TYPES;
                    $FILES = array();
                    if ($handler = opendir (SQL_DIR)) {
                        echo "<B>Available patches:</B><BR>";

                        $folder = array();
                        while (false !== ($file_item = readdir ($handler))) {
                                $folder[] = $file_item;
                        }
                        sort($folder);
                        
                        foreach ($folder as $file) {
                            if (is_file (SQL_DIR . $file) && !in_array ($file, $FILES_TO_SKIP) && !preg_match ("~_APPLIED~", $file)) {
                                $patch_info = get_patch_info ($file);
                                $checked = '';

                                if (isset ($_GET['files_to_exec']) && in_array ($file, $_GET['files_to_exec'])) {
                                    $checked = 'checked="checked"';
                                }

                                if (!isset ($_GET['files_to_exec']) &&
                                    in_array ($patch_info['patch_type'], $PRECHECKED_TYPES)
                                    && !$patch_info['is_applied']) {
                                    $checked = 'checked="checked"';
                                }

                                echo '<input type="checkbox" ' . $checked . ' class="' . $patch_info['patch_type'] . '" name="files_to_exec[]" value="' . $file . '" />';
                                if ($patch_info['is_applied']) {
                                    echo '<span style="color:grey;">' . $file . ' APPLIED</span>';
                                } elseif ($patch_info['error']) {
                                    echo '<span style="color:red;">' . $file . ' ERROR: ' . $patch_info['error'] . '</span>';
                                } else {
                                    echo '<span style="color:green;">' . $file . ' READY</span>';
                                }
                                if ($patch_info['comment']) {
                                    echo '&nbsp;&nbsp;&nbsp;<span style="color:grey;">comment: ' . $patch_info['comment'] . '</span>';
                                }
                                echo "<BR>";
                                $FILES[] = $file;
                            }
                        }
                        closedir ($handler);
                        echo "<hr>";
                    } else {
                        echo "Can't open dir with SQL patches: " . SQL_DIR;
                        exit;
                    }
                    return $FILES;
                }

                function get_patch_info ($filename)
                {
                    global $fileTypes;
                    $patch_info = array('filename' => $filename, 'is_applied' => false, 'error' => false);
                    if (!preg_match (SQL_FILENAME_PATTERN, $filename, $file_info)) {
                        $patch_info['error'] = "Patch file has wrong name. See pattern: [year][month][day][hourminute].[" . implode ("|", $fileTypes) . "].[author's login].sql";
                    }
                    $patch_info['patch_type'] = $file_info[1];
                    $patch_info['sql'] = file_get_contents (SQL_DIR . $filename);

                    if (preg_match ("~/\*comment:(.+?)\*/~", $patch_info['sql'], $comments_match)) {
                        $patch_info['comment'] = $comments_match[1];
                    } else
                        $patch_info['comment'] = '';
                    if (is_file (SQL_DIR . str_replace ('.sql', '_APPLIED.sql', $filename))) {
                        $patch_info['is_applied'] = true;
                    }
                    return $patch_info;
                }

                /**
                 *
                 * Function check does sql patch file is in correct format
                 * If sql patch file name is in wrong format or file already is applied function returns false
                 * In other cases it returns true
                 *
                 */
                function patch_availible ($file)
                {
                    if (!preg_match (SQL_FILENAME_PATTERN, $file, $file_info)) {
                        echo "<span style='color:red;'>" . $file . "</span>: Patch file has wrong name. See pattern: [month][day][hour][minute].[" . implode ("|", $fileTypes) . "].[author's login].sql<BR>";
                        return false;
                    }

                    $sql = file_get_contents (SQL_DIR . $file);

                    if (!isset ($_GET['files_to_exec']) || !in_array ($file, $_GET['files_to_exec'])) {
                        echo "<span style='color:grey;'>" . $file . "</span>: This patch shouldn't be applied.<BR>";
                        return false;
                    }

                    if (empty ($sql)) {
                        echo "<span style='color:red;'>" . $file . "</span>: Patch file has no content.<BR>";
                        return false;
                    }
                    /*
                      if(is_file(SQL_DIR . str_replace('.sql', '_APPLIED.sql', $file))){
                      echo "<span style='color:grey;'>" . $file . "</span>: This patch already applied.<BR>";
                      return false;
                      } */

                    return $sql;
                }

                /**
                 *
                 * This function gets and string with SQL and apply it to database;
                 * Also it sets "APPLIED" comment to patch file
                 * If you apply your patch manually you must set this comment manually also
                 *
                 */
                function apply_patch ($full_sql, $file)
                {
                    echo "<span style='color:green;'>" . $file . "</span>: Patch applied.<BR>";

                    $SQL = preg_replace ("#/\*.+?\*/#ms", '', $full_sql);
                    $SQLs = preg_split ("~;\s*[\r\n]+~", $SQL);

                    mysql_query ("SET NAMES utf8");
                    foreach ($SQLs as $sql) {
                        $sql = trim ($sql);
                        //print '+'.$sql.'+<br /><br /><br /><br />';
                        if ($sql) {
                            mysql_query ($sql);
                            if (mysql_error ()) {
                                echo "<div style='border:1px solid red; padding:10px; margin-bottom:5px;'><span style='color:red; font-weight:bold;'>ERROR has occured when tried to apply this patch:</span> " . mysql_error () . "</div>";
                            }
                        }
                    }

                    //Create special 'mark' file to set that this patch applied
                    $mark_filename = str_replace ('.sql', '_APPLIED.sql', $file);
                    $mf = fopen (SQL_DIR . $mark_filename, 'w') or die ("can't open file");
                    fclose ($mf);
                }
                ?>
            </div>
        </div>

    </body>
</html>

